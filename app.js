var budgetController = (function () {

    var Expense = function (id, description,value) {
        this.id = id;
        this.value = value;
        this.description = description;
        this.percentage = -1;
    };
    
    Expense.prototype.calcPercentage = function(totalIncome) {

        if(totalIncome > 0) {
            this.percentage = Math.round((this.value / totalIncome) * 100);
        } else {
            this.percentage = -1;
        }
    };
    
    Expense.prototype.getPercentage = function() {
      return this.percentage;  
    };

    var Income  = function (id, description, value) {
        this.id = id;
        this.value = value;
        this.description = description;
    };

    var calculateTotal = function (type) {
          var sum = 0;

          data.allItems[type].forEach(function (current) {
              sum += current.value;
          });

          data.totals[type] = sum;
    };
    
    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1
    };

        return{
            addItem: function (type, description, value) {
                var newItem, ID;

                //Create new ID
                if(data.allItems[type].length > 0){
                    ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
                }
                else{
                    ID = 0;
                }

                //Create new Item base on inc or exp type
                if(type === 'exp'){
                    newItem = new Expense(ID, description, value);
                }else if(type === 'inc'){
                    newItem = new Income(ID, description, value);
                }

                //push it into the data structure
                data.allItems[type].push(newItem);

                //return the new element
                return newItem;
            },
            
            deleteItem : function(type, id) {
                var index;
                
                var ids = data.allItems[type].map(function(current) {
                   return current.id; 
                });
                
                index = ids.indexOf(id);
                
                if(index !== -1) {
                    data.allItems[type].splice(index, 1);
                }
            },

            calculateBudget: function () {
                 //TODO calculate total income and expenses
                calculateTotal('exp');
                calculateTotal('inc');

                //TODO calculate the budget : income - expenses
                data.budget = data.totals.inc - data.totals.exp;

                //TODO calculate the percentage of income that we spent
                if(data.totals.inc > 0){
                    data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
                }else{
                    data.percentage = -1;
                }

            },
            
            calculatePercentage: function() {
                
                data.allItems.exp.forEach(function(current) {
                   current.calcPercentage(data.totals.inc); 
                });
                
            },
            
            getPercentages: function() {
              var allPercentages = data.allItems.exp.map(function(current) {
                 return current.getPercentage(); 
              });
                
                return allPercentages;
            },

            getBudget: function () {
                return{
                    budget: data.budget,
                    totalInc: data.totals.inc,
                    totalExp: data.totals.exp,
                    percentage: data.percentage
                };
            },

            test: function () {
                console.log(data);
            }
        };

})();


var UIController = (function () {

    var DOMStrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expensesContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        budgetTitleMonth: '.budget__title',
        container: '.container',
        expensesPercentLabel: '.item__percentage',
        dateLabel: '.budget__title--month'
    };
    
    var formatNumber = function(num, type) {
            var numSplit, int, decimal;
            
            num = Math.abs(num);
            num = num.toFixed(2);
            
            numSplit = num.split('.');
            int = numSplit[0];
            decimal = numSplit[1];
            
            if(int.length > 3) {
                int = int.substr(0, int.length - 3) + ',' + int.substr(int.length - 3, 3);
            }
                        
            return (type === 'exp' ? '-' : '+') + ' ' + int + '.' + decimal;
        };
    
    var nodeListForEach = function(list, callback) {
                for(var i = 0; i < list.length; i++) {
                    callback(list[i], i);
                }
            };

    return{
        getInput: function () {
            return{

            type: document.querySelector(DOMStrings.inputType).value, // will be either inc or exp
            description: document.querySelector(DOMStrings.inputDescription).value,
            value: parseFloat(document.querySelector(DOMStrings.inputValue).value)
            };
        },

        addListItem: function (obj, type) {
            //TODO create HTML string with placeHolder text
            var html, newHtml, element;

            if(type === 'inc'){
                element = DOMStrings.incomeContainer;
                html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div> <div class="item__delete"> <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button> </div> </div> </div>';
            }else if(type === 'exp'){
                element = DOMStrings.expensesContainer;
                html = '<div class="item clearfix" id="exp-%id%"> <div class="item__description">%description%</div> <div class="right clearfix"> <div class="item__value">%value%</div> <div class="item__percentage">21%</div> <div class="item__delete"> <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button> </div> </div> </div>';
            }


            //TODO replace the placeHolder text with the actual data
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', formatNumber(obj.value, type));


            //TODO insert the HTML into the DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);

        },
        
        deleteListItem: function(selectorID) {
            var elemetToDeleted;
            
            elemetToDeleted = document.getElementById(selectorID);
            elemetToDeleted.parentNode.removeChild(elemetToDeleted);
        },

        clearFields: function () {
            var fields, fieldsArr;


            fields = document.querySelectorAll(DOMStrings.inputDescription + ', ' + DOMStrings.inputValue);
            fieldsArr = Array.prototype.slice.call(fields);
            fieldsArr.forEach(function (current, index, array) {
                current.value = '';
            });



            fieldsArr[0].focus();
        },

        displayBudget: function (obj) {
            var type;
            
            type = obj.budget > 0 ? 'inc' : 'exp';
            document.querySelector(DOMStrings.budgetLabel).textContent = formatNumber(obj.budget, type);
            document.querySelector(DOMStrings.expensesLabel).textContent = formatNumber(obj.totalExp, 'exp');
            document.querySelector(DOMStrings.incomeLabel).textContent = formatNumber(obj.totalInc, 'inc');
            

            if(obj.percentage > 0){
                document.querySelector(DOMStrings.percentageLabel).textContent = obj.percentage + '%';
            }else{
                document.querySelector(DOMStrings.percentageLabel).textContent = '---';
            }
        },
        
        displayPercentages: function(percentages) {
            var fields = document.querySelectorAll(DOMStrings.expensesPercentLabel);
            
            
            
            nodeListForEach(fields, function(current, index) {
                if(percentages[index] > 0) {
                    current.textContent = percentages[index]  + '%';
                } else {
                    current.textContent = '---';
                }
            });

        },
        
        displayMonthAndYear: function() {
            var now, month, year, months;
            var months = ['Januar', 'Februar', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            
            now = new Date();
            month = now.getMonth();
            year = now.getFullYear();
            document.querySelector(DOMStrings.dateLabel).textContent = months[month] + ' ' + year;
        },
        
        changedType: function() {
          
            var fields = document.querySelectorAll(
            DOMStrings.inputType + ',' +
            DOMStrings.inputDescription + ',' +
            DOMStrings.inputValue);
            
            nodeListForEach(fields, function(cur) {
                cur.classList.toggle('red-focus');
            });
            
            document.querySelector(DOMStrings.inputBtn).classList.toggle('red');
        },
        
        getDOMStrings: function () {
            return DOMStrings;
        }
    };
    
})();


//Global app controller
var controller = (function (budgetCtrl, UICtrl) {
    
    var setupEventListeners = function () {

        var DOM = UICtrl.getDOMStrings();
        document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);

        document.addEventListener('keypress', function (event) {

            var key = event.keyCode || event.which;

            if(key === 13){
                ctrlAddItem();
            }
        });
        
        document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);
        
        document.querySelector(DOM.inputType).addEventListener('change', UICtrl.changedType);
    };

    var updateBudget = function () {

        // TODO 1. calculate the budget
        budgetCtrl.calculateBudget();
        // TODO 2. return the budget
        var budget = budgetCtrl.getBudget();
        // TODO 3. update the budget on the UI
        UICtrl.displayBudget(budget);
    };
    
    var updatePercentages = function() {
        budgetCtrl.calculatePercentage();
        var percentages = budgetCtrl.getPercentages();
        UICtrl.displayPercentages(percentages);
    };

    var ctrlAddItem = function () {
        var input, newItem;
         // TODO 1. Get the input data.
        input = UICtrl.getInput();
        if(input.description !== '' && !isNaN(input.value) && input.value > 0){
            // TODO 2. Add the item to the budget controller
            newItem = budgetCtrl.addItem(input.type, input.description, input.value);

            // TODO 3. add the new item to te UI

            UICtrl.addListItem(newItem, input.type);
            UICtrl.clearFields();
            updateBudget();
            
            //4. calculate and update the percentages
            updatePercentages();
        }
    };
    
    var ctrlDeleteItem = function(e) {
        var itemID, splitID, type, ID, typeIndex = 0, IDIndex = 1;
        
        itemID = e.target.parentNode.parentNode.parentNode.parentNode.id;
        
        if(itemID) {
            splitID = itemID.split('-');
            type = splitID[typeIndex];
            ID = parseInt(splitID[IDIndex]);
            budgetCtrl.deleteItem(type, ID);
            UICtrl.deleteListItem(itemID);
            updateBudget();
            //. calculate and update the percentages
            updatePercentages();
        }
    };

    return{
        init: function () {
            console.log('%c Application has stated!!', 'background: #222; color: #a1ff54');

            UICtrl.displayMonthAndYear();

            UICtrl.displayBudget({
                budget: 0,
                totalInc: 0,
                totalExp: 0,
                percentage: -1
            });
            setupEventListeners();
        }
    }
})(budgetController, UIController);

controller.init();